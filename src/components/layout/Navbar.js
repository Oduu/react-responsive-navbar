import React from 'react';
import '../../css/layout/Navbar.css';
import Burger from './Burger';

export default class Navbar extends React.Component {
    
    render() {
        return (
            <div id="navbar">
                <div className="details">
                    
                    <h1>Navbar</h1>
                </div>

                {/* also contains nav links */}
                <Burger />

                
            </div>
        );
    }
}
