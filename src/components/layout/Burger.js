import React, { Component } from 'react';
import '../../css/layout/Burger.css';
import RightNav from './RightNav';

export default class Burger extends Component {

	constructor(props) {
		super(props);
		this.toggleMenu = this.toggleMenu.bind(this);
	}

	state = {
		toggleMenu: false,
	}

	toggleMenu() {
		let toggleMenu = this.state.toggleMenu;
		toggleMenu = !toggleMenu;
		this.setState({ toggleMenu });
	}

	render() {

		const renderButton = () => {

			if(this.state.toggleMenu) {
				
				return <div className="burger-button-div" onClick={this.toggleMenu}><i className="burger-button fa fa-times fa-2x" /></div>
			} else {
				
				return <div onClick={this.toggleMenu}><i className="burger-button fa fa-bars fa-2x" /></div>
			}
		}



		return (
			<div className="burger-menu">

        		{renderButton()}
        		
        		<RightNav open={this.state.toggleMenu} toggleMenu={this.toggleMenu} />
			</div>
		);
	}
}
