import React from 'react';
import './App.css';
import Home from './components/Home';
import { BrowserRouter as Router, Route, Switch  } from 'react-router-dom';
import Navbar from './components/layout/Navbar';

function App() {
  return (
      <Router basename={`${process.env.PUBLIC_URL}`}>
        <Navbar />
        <div className="App" >
        
          <Switch>
            <Route exact path="/" component={Home} />

            <Route exact path="/about" component={Home} />

            <Route path="/education" component={Home} />

            <Route path="/skills" component={Home} />

            <Route component={Home} />
            
          </Switch>
          
        </div>

      </Router>
  );
}

export default App;
